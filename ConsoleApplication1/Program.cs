﻿using ConsoleApplication1.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            //List<string> stList = new List<string>();
            //StreamReader sr = new StreamReader("metinAzerbaycan.txt");
            //string satir;
            //int say;
            //while ((satir = sr.ReadLine()) != null)
            //{
            //    stList.Add("insert into city (name,country_id) values ('" + satir + "',15);");
            //    Console.WriteLine("insert into city (name,country_id) values ('" + satir + "',15);");

            //}
            //sr.Close();
            //StreamWriter sw = new StreamWriter("Azerbaycan.sql");
            //foreach (var item in stList)
            //{
            //    sw.WriteLine(item);
            //}
            //sw.Close();

            //string elma = County().Result;
            int i = 0;
            StreamReader sr = new StreamReader("country.txt");

            string str = sr.ReadToEnd();
            string[] dizim = str.Split('\n');
            string path = "newCountry.sql";
            FileStream fs = new FileStream(path, FileMode.CreateNew);
            StreamWriter swCounty = new StreamWriter(fs, Encoding.UTF8, 512);
            foreach (var item in dizim)
            {
                if (item != "")
                {


                    string ulkeid = item.Split(',')[0] != "" ? item.Split(',')[0] : "";
                    string ulke = item.Split(',')[1] != "" ? item.Split(',')[1] : "";
                    string ulketemizad = ulke.Split('\r')[0] != "" ? ulke.Split('\r')[0].ToString() : "";
                    swCounty.WriteLine("INSERT INTO country (id, name_en, name_tr) VALUES('" + item.Split(',')[0] + "," + ulketemizad + "," + item.Split(',')[2] + "');");
                    Console.WriteLine("INSERT INTO country (id, name_en, name_tr) VALUES('" + item.Split(',')[0] + "," + ulketemizad + "," + item.Split(',')[2] + "');");

                    //string ulkeid = item.Split(',')[0] != null ? item.Split(',')[0] : "";
                    //string ulke = item.Split(',')[1] != null ? item.Split(',')[1] : "";
                    //string ulketemizad = ulke.Split('\r')[0] != null ? ulke.Split('\r')[0].ToString() : "";
                    //MyModel portakal = City(ulke).Result;
                    //StreamWriter swcities = new StreamWriter(ulketemizad + ".sql");
                    //foreach (var jitem in portakal.details.regionalBlocs)
                    //{
                    //    swcities.WriteLine("insert into city (name,country_id) values ('" + jitem.state_name + "'," + ulkeid + ");");
                    //    Console.WriteLine("insert into city (name,country_id) values ('" + jitem.state_name + "'," + ulkeid + ");");
                    //}
                    //swcities.Close();
                }
            }
            //foreach (var item in elma.country_name)
            //{
            //    i++;
            //    Console.WriteLine("insert into country (id,name_en,name_tr) values ('" + i + item + "'," + "###############" + ");");
            //    //string das = item.ToString();
            //    //MyModel portakal = City(item).Result;
            //    //StreamWriter sw = new StreamWriter(item.country_name + ".sql");
            //    //foreach (var jitem in portakal.details.regionalBlocs)
            //    //{
            //    //    Console.WriteLine("insert into city (name,country_id) values ('" + jitem.state_name + "'," + i + ");");
            //    //}
            //}
            Console.WriteLine("Ulke Adı: \r\n");
            string ulkeadi = Console.ReadLine();
            Console.WriteLine("Ulke Kodu Veri Tabanında ki : \r\n");
            string ulkekodu = Console.ReadLine();
            MyModel d = City(ulkeadi).Result;
            StreamWriter sw = new StreamWriter(ulkeadi + ".sql");
            foreach (var item in d.details.regionalBlocs)
            {
                Console.WriteLine("insert into city (name,country_id) values ('" + item.state_name + "'," + ulkekodu + ");");
                sw.WriteLine("insert into city (name,country_id) values ('" + item.state_name + "'," + ulkekodu + ");");
            }
            sw.Close();
            Console.ReadLine();


        }
        static async Task<string> County()
        {
            RootObject don = new RootObject();
            List<RootObject> detail = new List<RootObject>();
            string Baseurl2 = "https://geodata.solutions/";


            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("restapi?country=");

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    //Deserializing the response 

                    //IEnumerable xxxxx = (IEnumerable)JsonConvert.DeserializeObject(EmpResponse);
                    JObject ss = JObject.Parse(EmpResponse);
                    int i = 0;
                    StreamWriter sr = new StreamWriter("country.txt");
                    foreach (var item in ss)
                    {
                        i++;
                        sr.WriteLine(i + "," + item.Value["country_name"]);
                        Console.WriteLine(item.Value["country_name"]);
                        //detail.Add(new RootObject { country_name = item.Value["country_name"].ToString() });
                    }
                    sr.Close();


                }
                //Console.ReadLine();
            }
            return "";
        }

        static async Task<MyModel> City(string ulkeadi)
        {
            MyModel detail = new MyModel();
            string Baseurl2 = "https://geodata.solutions/";


            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("restapi?country=" + ulkeadi + "");

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;
                    //Deserializing the response recieved from web api and storing into the Employee list

                    detail = JsonConvert.DeserializeObject<MyModel>(EmpResponse);
                    //var deneme = JsonConvert.DeserializeObject<LeagueStageMain>(EmpResponse);

                    //foreach (var item in detail.details.regionalBlocs)
                    //{
                    //    Console.WriteLine(item.state_name);
                    //}

                }
                //Console.ReadLine();
            }
            return detail;
        }
    }
}
