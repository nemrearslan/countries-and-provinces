﻿INSERT INTO country (id, name_en, name_tr) VALUES('1,Afghanistan,Afganistan');
INSERT INTO country (id, name_en, name_tr) VALUES('2,Albania,Arnavutluk');
INSERT INTO country (id, name_en, name_tr) VALUES('3,Algeria,Cezayir');
INSERT INTO country (id, name_en, name_tr) VALUES('4,Andorra,Andorra');
INSERT INTO country (id, name_en, name_tr) VALUES('5,Angola,Angola');
INSERT INTO country (id, name_en, name_tr) VALUES('6,Anguilla,Anguilla');
INSERT INTO country (id, name_en, name_tr) VALUES('7,Antigua and Barbuda,Antigua ve Barbuda');
INSERT INTO country (id, name_en, name_tr) VALUES('8,Argentina,Arjantin');
INSERT INTO country (id, name_en, name_tr) VALUES('9,Armenia,Ermenistan');
INSERT INTO country (id, name_en, name_tr) VALUES('10,Australia,Avustralya');
INSERT INTO country (id, name_en, name_tr) VALUES('11,Austria,Avusturya');
INSERT INTO country (id, name_en, name_tr) VALUES('12,Azerbaijan,Azerbaycan');
INSERT INTO country (id, name_en, name_tr) VALUES('13,Bahamas,Bahamalar');
INSERT INTO country (id, name_en, name_tr) VALUES('14,Bahrain,Bahreyn');
INSERT INTO country (id, name_en, name_tr) VALUES('15,Bangladesh,Banglades');
INSERT INTO country (id, name_en, name_tr) VALUES('16,Barbados,Barbados');
INSERT INTO country (id, name_en, name_tr) VALUES('17,Belarus,Beyaz Rusya');
INSERT INTO country (id, name_en, name_tr) VALUES('18,Belgium,Belçika');
INSERT INTO country (id, name_en, name_tr) VALUES('19,Belize,Belize');
INSERT INTO country (id, name_en, name_tr) VALUES('20,Benin,Benin');
INSERT INTO country (id, name_en, name_tr) VALUES('21,Bermuda,Bermuda');
INSERT INTO country (id, name_en, name_tr) VALUES('22,Bhutan,Bhutan');
INSERT INTO country (id, name_en, name_tr) VALUES('23,Bolivia,Bolivya');
INSERT INTO country (id, name_en, name_tr) VALUES('24,Bosnia and Herzegovina,Bosna-Hersek');
INSERT INTO country (id, name_en, name_tr) VALUES('25,Botswana,Botsvana');
INSERT INTO country (id, name_en, name_tr) VALUES('26,Brazil,Brezilya');
INSERT INTO country (id, name_en, name_tr) VALUES('27,Brunei Darussalam,Brunei');
INSERT INTO country (id, name_en, name_tr) VALUES('28,Bulgaria,Bulgaristan');
INSERT INTO country (id, name_en, name_tr) VALUES('29,Burkina Faso,Burkina Faso');
INSERT INTO country (id, name_en, name_tr) VALUES('30,Burundi,Burundi');
INSERT INTO country (id, name_en, name_tr) VALUES('31,Cambodia,Kamboçya');
INSERT INTO country (id, name_en, name_tr) VALUES('32,Cameroon,Kamerun');
INSERT INTO country (id, name_en, name_tr) VALUES('33,Canada,Kanada');
INSERT INTO country (id, name_en, name_tr) VALUES('34,Cape Verde,Yesil Burun Adalari');
INSERT INTO country (id, name_en, name_tr) VALUES('35,Cayman Islands,Cayman Adalari');
INSERT INTO country (id, name_en, name_tr) VALUES('36,Central African Republic,Orta Afrika Cumhuriyeti');
INSERT INTO country (id, name_en, name_tr) VALUES('37,Chad,Çad');
INSERT INTO country (id, name_en, name_tr) VALUES('38,Chile,Sili');
INSERT INTO country (id, name_en, name_tr) VALUES('39,China,Çin');
INSERT INTO country (id, name_en, name_tr) VALUES('40,Colombia,Kolombiya');
INSERT INTO country (id, name_en, name_tr) VALUES('41,Comoros,Komorlar');
INSERT INTO country (id, name_en, name_tr) VALUES('42,Congo,Kongo Cumhuriyeti');
INSERT INTO country (id, name_en, name_tr) VALUES('43,Costa Rica,Kosta Rika');
INSERT INTO country (id, name_en, name_tr) VALUES('44,Croatia (Hrvatska),Hirvatistan');
INSERT INTO country (id, name_en, name_tr) VALUES('45,Cuba,Küba');
INSERT INTO country (id, name_en, name_tr) VALUES('46,Cyprus,Kibris Cumhuriyeti');
INSERT INTO country (id, name_en, name_tr) VALUES('47,Czech Republic,Çek Cumhuriyeti');
INSERT INTO country (id, name_en, name_tr) VALUES('48,Denmark,Danimarka');
INSERT INTO country (id, name_en, name_tr) VALUES('49,Djibouti,Cibuti');
INSERT INTO country (id, name_en, name_tr) VALUES('50,Dominica,Dominika');
INSERT INTO country (id, name_en, name_tr) VALUES('51,Dominican Republic,Dominik Cumhuriyeti');
INSERT INTO country (id, name_en, name_tr) VALUES('52,Ecuador,Ekvador');
INSERT INTO country (id, name_en, name_tr) VALUES('53,Egypt,Misir');
INSERT INTO country (id, name_en, name_tr) VALUES('54,El Salvador,El Salvador');
INSERT INTO country (id, name_en, name_tr) VALUES('55,Equatorial Guinea,Ekvator Ginesi');
INSERT INTO country (id, name_en, name_tr) VALUES('56,Eritrea,Eritre');
INSERT INTO country (id, name_en, name_tr) VALUES('57,Estonia,Estonya');
INSERT INTO country (id, name_en, name_tr) VALUES('58,Ethiopia,Etiyopya');
INSERT INTO country (id, name_en, name_tr) VALUES('59,Faroe Islands,Faroe Adalari');
INSERT INTO country (id, name_en, name_tr) VALUES('60,Fiji,Fiji');
INSERT INTO country (id, name_en, name_tr) VALUES('61,Finland,Finlandiya');
INSERT INTO country (id, name_en, name_tr) VALUES('62,France,Fransa');
INSERT INTO country (id, name_en, name_tr) VALUES('63,French Guiana,Fransiz Guyanasi');
INSERT INTO country (id, name_en, name_tr) VALUES('64,French Polynesia,Fransiz Polinezyasi');
INSERT INTO country (id, name_en, name_tr) VALUES('65,French Southern Territories,Fransiz Güney ve Antarktika Topraklari');
INSERT INTO country (id, name_en, name_tr) VALUES('66,Gabon,Gabon');
INSERT INTO country (id, name_en, name_tr) VALUES('67,Gambia,Gambiya');
INSERT INTO country (id, name_en, name_tr) VALUES('68,Georgia,Gürcistan');
INSERT INTO country (id, name_en, name_tr) VALUES('69,Germany,Almanya');
INSERT INTO country (id, name_en, name_tr) VALUES('70,Ghana,Gana');
INSERT INTO country (id, name_en, name_tr) VALUES('71,Greece,Yunanistan');
INSERT INTO country (id, name_en, name_tr) VALUES('72,Greenland,Grönland');
INSERT INTO country (id, name_en, name_tr) VALUES('73,Grenada,Grenada');
INSERT INTO country (id, name_en, name_tr) VALUES('74,Guadeloupe,Guadeloupe');
INSERT INTO country (id, name_en, name_tr) VALUES('75,Guam,Guam');
INSERT INTO country (id, name_en, name_tr) VALUES('76,Guatemala,Guatemala');
INSERT INTO country (id, name_en, name_tr) VALUES('77,Guinea,Gine');
INSERT INTO country (id, name_en, name_tr) VALUES('78,Guinea-Bissau,Gine-Bissau');
INSERT INTO country (id, name_en, name_tr) VALUES('79,Guyana,Guyana');
INSERT INTO country (id, name_en, name_tr) VALUES('80,Haiti,Haiti');
INSERT INTO country (id, name_en, name_tr) VALUES('81,Honduras,Honduras');
INSERT INTO country (id, name_en, name_tr) VALUES('82,Hong Kong,Hong Kong');
INSERT INTO country (id, name_en, name_tr) VALUES('83,Hungary,Macaristan');
INSERT INTO country (id, name_en, name_tr) VALUES('84,Iceland,Izlanda');
INSERT INTO country (id, name_en, name_tr) VALUES('85,India,Hindistan');
INSERT INTO country (id, name_en, name_tr) VALUES('86,Isle of Man,Man Adasi');
INSERT INTO country (id, name_en, name_tr) VALUES('87,Indonesia,Endonezya');
INSERT INTO country (id, name_en, name_tr) VALUES('88,Iran,Iran');
INSERT INTO country (id, name_en, name_tr) VALUES('89,Iraq,Irak');
INSERT INTO country (id, name_en, name_tr) VALUES('90,Ireland,Irlanda');
INSERT INTO country (id, name_en, name_tr) VALUES('91,Israel,Israil');
INSERT INTO country (id, name_en, name_tr) VALUES('92,Italy,Italya');
INSERT INTO country (id, name_en, name_tr) VALUES('93,Ivory Coast,Fildisi Sahili');
INSERT INTO country (id, name_en, name_tr) VALUES('94,Jersey,Jersey');
INSERT INTO country (id, name_en, name_tr) VALUES('95,Jamaica,Jamaika');
INSERT INTO country (id, name_en, name_tr) VALUES('96,Japan,Japonya');
INSERT INTO country (id, name_en, name_tr) VALUES('97,Jordan,Ürdün');
INSERT INTO country (id, name_en, name_tr) VALUES('98,Kazakhstan,Kazakistan');
INSERT INTO country (id, name_en, name_tr) VALUES('99,Kenya,Kenya');
INSERT INTO country (id, name_en, name_tr) VALUES('100,Kiribati,Kiribati');
INSERT INTO country (id, name_en, name_tr) VALUES('101,North Korea,Kuzey Kore');
INSERT INTO country (id, name_en, name_tr) VALUES('102,South Korea,Güney Kore');
INSERT INTO country (id, name_en, name_tr) VALUES('103,Kosovo,Kosova');
INSERT INTO country (id, name_en, name_tr) VALUES('104,Kuwait,Kuveyt');
INSERT INTO country (id, name_en, name_tr) VALUES('105,Kyrgyzstan,Kirgizistan');
INSERT INTO country (id, name_en, name_tr) VALUES('106,Laos,Loas');
INSERT INTO country (id, name_en, name_tr) VALUES('107,Latvia,Letonya');
INSERT INTO country (id, name_en, name_tr) VALUES('108,Lebanon,Lübnan');
INSERT INTO country (id, name_en, name_tr) VALUES('109,Lesotho,Lesotho');
INSERT INTO country (id, name_en, name_tr) VALUES('110,Liberia,Liberya');
INSERT INTO country (id, name_en, name_tr) VALUES('111,Libyan Arab Jamahiriya,Libya');
INSERT INTO country (id, name_en, name_tr) VALUES('112,Liechtenstein,Lihtenstayn');
INSERT INTO country (id, name_en, name_tr) VALUES('113,Lithuania,Litvanya');
INSERT INTO country (id, name_en, name_tr) VALUES('114,Luxembourg,Lüksemburg');
INSERT INTO country (id, name_en, name_tr) VALUES('115,Macedonia,Makedonya Cumhuriyeti');
INSERT INTO country (id, name_en, name_tr) VALUES('116,Madagascar,Madagaskar');
INSERT INTO country (id, name_en, name_tr) VALUES('117,Malawi,Malavi');
INSERT INTO country (id, name_en, name_tr) VALUES('118,Malaysia,Malezya');
INSERT INTO country (id, name_en, name_tr) VALUES('119,Maldives,Maldivler');
INSERT INTO country (id, name_en, name_tr) VALUES('120,Mali,Mali');
INSERT INTO country (id, name_en, name_tr) VALUES('121,Malta,Malta');
INSERT INTO country (id, name_en, name_tr) VALUES('122,Marshall Islands,Marshall Adalari');
INSERT INTO country (id, name_en, name_tr) VALUES('123,Martinique,Martinique');
INSERT INTO country (id, name_en, name_tr) VALUES('124,Mauritania,Moritanya');
INSERT INTO country (id, name_en, name_tr) VALUES('125,Mauritius,Mauritius');
INSERT INTO country (id, name_en, name_tr) VALUES('126,Mexico,Meksika');
INSERT INTO country (id, name_en, name_tr) VALUES('127,Micronesia,Mikronezya Federal Devletleri');
INSERT INTO country (id, name_en, name_tr) VALUES('128,Moldova,Moldova');
INSERT INTO country (id, name_en, name_tr) VALUES('129,Monaco,Monako');
INSERT INTO country (id, name_en, name_tr) VALUES('130,Mongolia,Mogolistan');
INSERT INTO country (id, name_en, name_tr) VALUES('131,Montenegro,Karadag');
INSERT INTO country (id, name_en, name_tr) VALUES('132,Montserrat,Montserrat');
INSERT INTO country (id, name_en, name_tr) VALUES('133,Morocco,Fas');
INSERT INTO country (id, name_en, name_tr) VALUES('134,Mozambique,Mozambik');
INSERT INTO country (id, name_en, name_tr) VALUES('135,Myanmar,Myanmar');
INSERT INTO country (id, name_en, name_tr) VALUES('136,Namibia,Namibya');
INSERT INTO country (id, name_en, name_tr) VALUES('137,Nauru,Nauru');
INSERT INTO country (id, name_en, name_tr) VALUES('138,Nepal,Nepal');
INSERT INTO country (id, name_en, name_tr) VALUES('139,Netherlands,Hollanda');
INSERT INTO country (id, name_en, name_tr) VALUES('140,Netherlands Antilles,Hollanda Antilleri');
INSERT INTO country (id, name_en, name_tr) VALUES('141,New Caledonia,Yeni Kaledonya');
INSERT INTO country (id, name_en, name_tr) VALUES('142,New Zealand,Yeni Zelanda');
INSERT INTO country (id, name_en, name_tr) VALUES('143,Nicaragua,Nikaragua');
INSERT INTO country (id, name_en, name_tr) VALUES('144,Niger,Nijer');
INSERT INTO country (id, name_en, name_tr) VALUES('145,Nigeria,Nijerya');
INSERT INTO country (id, name_en, name_tr) VALUES('146,Northern Mariana Islands,Kuzey Mariana Adalari');
INSERT INTO country (id, name_en, name_tr) VALUES('147,Norway,Norveç');
INSERT INTO country (id, name_en, name_tr) VALUES('148,Oman,Umman');
INSERT INTO country (id, name_en, name_tr) VALUES('149,Pakistan,Pakistan');
INSERT INTO country (id, name_en, name_tr) VALUES('150,Palau,Palau');
INSERT INTO country (id, name_en, name_tr) VALUES('151,Palestine,Filistin Devleti');
INSERT INTO country (id, name_en, name_tr) VALUES('152,Panama,Panama');
INSERT INTO country (id, name_en, name_tr) VALUES('153,Papua New Guinea,Papua Yeni Gine');
INSERT INTO country (id, name_en, name_tr) VALUES('154,Paraguay,Paraguay');
INSERT INTO country (id, name_en, name_tr) VALUES('155,Peru,Peru');
INSERT INTO country (id, name_en, name_tr) VALUES('156,Philippines,Filipinler');
INSERT INTO country (id, name_en, name_tr) VALUES('157,Poland,Polonya');
INSERT INTO country (id, name_en, name_tr) VALUES('158,Portugal,Portekiz');
INSERT INTO country (id, name_en, name_tr) VALUES('159,Puerto Rico,Porto Riko');
INSERT INTO country (id, name_en, name_tr) VALUES('160,Qatar,Katar');
INSERT INTO country (id, name_en, name_tr) VALUES('161,Reunion,Réunion');
INSERT INTO country (id, name_en, name_tr) VALUES('162,Romania,Romanya');
INSERT INTO country (id, name_en, name_tr) VALUES('163,Russian Federation,Rusya');
INSERT INTO country (id, name_en, name_tr) VALUES('164,Rwanda,Ruanda');
INSERT INTO country (id, name_en, name_tr) VALUES('165,Saint Kitts and Nevis,Saint Kitts ve Nevis');
INSERT INTO country (id, name_en, name_tr) VALUES('166,Saint Lucia,Saint Lucia');
INSERT INTO country (id, name_en, name_tr) VALUES('167,Saint Vincent and the Grenadines,Saint Vincent ve Grenadinler');
INSERT INTO country (id, name_en, name_tr) VALUES('168,Samoa,Samoa');
INSERT INTO country (id, name_en, name_tr) VALUES('169,San Marino,San Marino');
INSERT INTO country (id, name_en, name_tr) VALUES('170,Sao Tome and Principe,São Tomé ve Príncip');
INSERT INTO country (id, name_en, name_tr) VALUES('171,Saudi Arabia,Suudi Arabistan');
INSERT INTO country (id, name_en, name_tr) VALUES('172,Senegal,Senegal');
INSERT INTO country (id, name_en, name_tr) VALUES('173,Serbia,Sirbistan');
INSERT INTO country (id, name_en, name_tr) VALUES('174,Seychelles,Seyseller');
INSERT INTO country (id, name_en, name_tr) VALUES('175,Sierra Leone,Sierra Leone');
INSERT INTO country (id, name_en, name_tr) VALUES('176,Singapore,Singapur');
INSERT INTO country (id, name_en, name_tr) VALUES('177,Slovakia,Slovakya');
INSERT INTO country (id, name_en, name_tr) VALUES('178,Slovenia,Slovenya');
INSERT INTO country (id, name_en, name_tr) VALUES('179,Solomon Islands,Solomon Adalari');
INSERT INTO country (id, name_en, name_tr) VALUES('180,Somalia,Somali');
INSERT INTO country (id, name_en, name_tr) VALUES('181,South Africa,Güney Afrika');
INSERT INTO country (id, name_en, name_tr) VALUES('182,Spain,Ispanya');
INSERT INTO country (id, name_en, name_tr) VALUES('183,Sri Lanka,Sri Lanka');
INSERT INTO country (id, name_en, name_tr) VALUES('184,St. Helena,Saint Helena');
INSERT INTO country (id, name_en, name_tr) VALUES('185,St. Pierre and Miquelon,Saint Pierre ve Miquelon');
INSERT INTO country (id, name_en, name_tr) VALUES('186,Sudan,Sudan');
INSERT INTO country (id, name_en, name_tr) VALUES('187,Suriname,Surinam');
INSERT INTO country (id, name_en, name_tr) VALUES('188,Svalbard and Jan Mayen Islands,Svalbard');
INSERT INTO country (id, name_en, name_tr) VALUES('189,Swaziland,Svaziland');
INSERT INTO country (id, name_en, name_tr) VALUES('190,Sweden,Isveç');
INSERT INTO country (id, name_en, name_tr) VALUES('191,Switzerland,Isviçre');
INSERT INTO country (id, name_en, name_tr) VALUES('192,Syrian Arab Republic,Suriye');
INSERT INTO country (id, name_en, name_tr) VALUES('193,Taiwan,Tayvan');
INSERT INTO country (id, name_en, name_tr) VALUES('194,Tajikistan,Tacikistan');
INSERT INTO country (id, name_en, name_tr) VALUES('195,Tanzania,Tanzanya');
INSERT INTO country (id, name_en, name_tr) VALUES('196,Thailand,Tayland');
INSERT INTO country (id, name_en, name_tr) VALUES('197,Togo,Togo');
INSERT INTO country (id, name_en, name_tr) VALUES('198,Tokelau,Tokelau');
INSERT INTO country (id, name_en, name_tr) VALUES('199,Tonga,Tonga');
INSERT INTO country (id, name_en, name_tr) VALUES('200,Trinidad and Tobago,Trinidad ve Tobago');
INSERT INTO country (id, name_en, name_tr) VALUES('201,Tunisia,Tunus');
INSERT INTO country (id, name_en, name_tr) VALUES('202,Turkey,Türkiye');
INSERT INTO country (id, name_en, name_tr) VALUES('203,Turkmenistan,Türkmenistan');
INSERT INTO country (id, name_en, name_tr) VALUES('204,Tuvalu,Tuvalu');
INSERT INTO country (id, name_en, name_tr) VALUES('205,Uganda,Uganda');
INSERT INTO country (id, name